package practice.dsr.ru.weather.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import practice.dsr.ru.weather.R;

public class SettingsFragment extends PreferenceFragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }

}
