package practice.dsr.ru.weather.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.activities.MainActivity;
import practice.dsr.ru.weather.activities.WeatherDetails;
import practice.dsr.ru.weather.activities.WizardActivity;
import practice.dsr.ru.weather.adapters.LocationArrayAdapter;
import practice.dsr.ru.weather.callbacks.UpdateCaller;
import practice.dsr.ru.weather.callbacks.UpdateListener;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;

public class AllCitiesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private UpdateCaller updateCaller;
    private ListView lvLocations;
    private Repository repository;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final UpdateListener listener = new UpdateListener() {
        @Override
        public void onUpdated() {
            swipeRefreshLayout.setRefreshing(false);
        }
    };

    public AllCitiesFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repository = new Repository();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            updateCaller = (UpdateCaller) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onUpdateForecast");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_cities, container, false);

        List<Location> locationList = repository.findAllLocations();
        if (locationList.isEmpty()) {
            view = inflater.inflate(R.layout.fragment_no_cities, container, false);
            Button btnAdd = (Button) view.findViewById(R.id.no_cities_add_btn);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent addLocationIntent = new Intent(getContext(), WizardActivity.class);
                    getActivity().startActivityForResult(addLocationIntent, MainActivity.REQUEST_UPDATE);
                }
            });
        } else {
            lvLocations = (ListView) view.findViewById(R.id.fragments_all_list);
            LocationArrayAdapter adapter = new LocationArrayAdapter(getContext(), locationList);
            lvLocations.setAdapter(adapter);
            lvLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intentDetails = new Intent(getContext(), WeatherDetails.class);
                    Location location = (Location) lvLocations.getAdapter().getItem(i);
                    if (location != null) {
                        intentDetails.putExtra("id", location.getId());
                        getActivity().startActivityForResult(intentDetails, MainActivity.REQUEST_REFRESH_LIST);
                    }
                }
            });
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragments_all_refresher);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(R.color.lightBlue, R.color.lightGreen, R.color.lightOrange);
        }
        return view;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                updateCaller.onUpdateForecast(listener);
            }
        });
    }

}
