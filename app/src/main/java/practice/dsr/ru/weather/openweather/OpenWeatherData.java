package practice.dsr.ru.weather.openweather;

import java.util.Date;

public class OpenWeatherData {

    public Date dateTime;
    public double temperature;
    public String state;
    public String description;
    public String icon;
    public double windSpeed;
    public double windDegree;
    public double pressure;
    public int humidity;

}
