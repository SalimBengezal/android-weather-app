package practice.dsr.ru.weather.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

@DatabaseTable(tableName = Location.TABLE_NAME)
public class Location implements Serializable {

    public static final String FIELD_IS_FAVOURITE = "favourite";
    static final String TABLE_NAME = "location";
    private static final String FIELD_ID = "id";
    private static final String FIELD_CITY_CODE = "city_code";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_COUNTRY = "country";
    private static final String FIELD_LONGITUDE = "longitude";
    private static final String FIELD_LATITUDE = "latitude";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(columnName = FIELD_CITY_CODE, canBeNull = false)
    private int cityCode;
    @DatabaseField(index = true, columnName = FIELD_NAME)
    private String name;
    @DatabaseField(columnName = FIELD_LATITUDE)
    private double latitude;
    @DatabaseField(columnName = FIELD_LONGITUDE)
    private double longitude;
    @DatabaseField(columnName = FIELD_COUNTRY)
    private String country;
    @DatabaseField(columnName = FIELD_IS_FAVOURITE)
    private boolean isFavourite;

    public Location() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCityCode() {
        return cityCode;
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }
}
