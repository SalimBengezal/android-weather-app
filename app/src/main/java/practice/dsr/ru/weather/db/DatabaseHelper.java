package practice.dsr.ru.weather.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Forecast;
import practice.dsr.ru.weather.db.model.Location;


public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "weather.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TAG = DatabaseHelper.class.getName();

    private Dao<Location, Integer> locationDao = null;
    private Dao<CurrentWeather, Integer> weatherDao = null;
    private Dao<Forecast, Integer> forecastDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            Log.i(TAG, "Creating tables");
            TableUtils.createTable(connectionSource, Location.class);
            TableUtils.createTable(connectionSource, CurrentWeather.class);
            TableUtils.createTable(connectionSource, Forecast.class);
        } catch (SQLException e) {
            Log.e(TAG, "Can't createLocation table", e);
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

        try {
            Log.i(TAG, "Upgrading and dropping database");
            TableUtils.dropTable(connectionSource, Location.class, true);
            TableUtils.dropTable(connectionSource, CurrentWeather.class, true);
            TableUtils.dropTable(connectionSource, Forecast.class, true);
            onCreate(database, connectionSource);
        } catch (SQLException e) {
            Log.e(TAG, "Can't drop tables", e);
            throw new RuntimeException(e);
        }
    }

    public Dao<Location, Integer> getLocationDao() throws SQLException {
        if (locationDao == null) {
            locationDao = getDao(Location.class);
        }
        return locationDao;
    }

    public Dao<CurrentWeather, Integer> getWeatherDao() throws SQLException {
        if (weatherDao == null) {
            weatherDao = getDao(CurrentWeather.class);
        }
        return weatherDao;
    }

    public Dao<Forecast, Integer> getForecastDao() throws SQLException {
        if (forecastDao == null) {
            forecastDao = getDao(Forecast.class);
        }
        return forecastDao;
    }

    @Override
    public void close() {
        locationDao = null;
        weatherDao = null;
        forecastDao = null;
        super.close();
    }

}
