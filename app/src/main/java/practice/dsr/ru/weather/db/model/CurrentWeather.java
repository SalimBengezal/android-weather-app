package practice.dsr.ru.weather.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

@DatabaseTable(tableName = CurrentWeather.TABLE_NAME)
public class CurrentWeather implements Serializable {

    public static final String FIELD_LOCATION_ID = "location_id";
    static final String TABLE_NAME = "current_weather";
    private static final String FIELD_ID = "id";
    private static final String FIELD_DATETIME = "datetime";
    private static final String FIELD_TEMPERATURE = "temperature";
    private static final String FIELD_STATE = "state";
    private static final String FIELD_DESCRIPTION = "description";
    private static final String FIELD_ICON = "icon";
    private static final String FIELD_WIND_SPEED = "wind_speed";
    private static final String FIELD_WIND_DEGREE = "wind_degree";
    private static final String FIELD_PRESSURE = "pressure";
    private static final String FIELD_HUMIDITY = "humidity";

    @DatabaseField(generatedId = true, columnName = FIELD_ID)
    private int id;
    @DatabaseField(canBeNull = false, unique = true, columnName = FIELD_LOCATION_ID)
    private int locationId;
    @DatabaseField(columnName = FIELD_DATETIME)
    private Date dateTime;
    @DatabaseField(columnName = FIELD_TEMPERATURE)
    private double temperature;
    @DatabaseField(columnName = FIELD_STATE)
    private String state;
    @DatabaseField(columnName = FIELD_DESCRIPTION)
    private String description;
    @DatabaseField(columnName = FIELD_ICON)
    private String icon;
    @DatabaseField(columnName = FIELD_WIND_SPEED)
    private double windSpeed;
    @DatabaseField(columnName = FIELD_WIND_DEGREE)
    private double windDegree;
    @DatabaseField(columnName = FIELD_PRESSURE)
    private double pressure;
    @DatabaseField(columnName = FIELD_HUMIDITY)
    private int humidity;

    public CurrentWeather() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public double getWindDegree() {
        return windDegree;
    }

    public void setWindDegree(double windDegree) {
        this.windDegree = windDegree;
    }

    public double getPressure() {
        return pressure;
    }

    public void setPressure(double pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
