package practice.dsr.ru.weather.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;
import practice.dsr.ru.weather.fragments.wizard.AddLocationFragment;
import practice.dsr.ru.weather.fragments.wizard.MapsFragment;

public class WizardActivity extends AppCompatActivity implements AddLocationFragment.LocationNameListener, MapsFragment.CoordinateListener {

    private Repository repository;

    private ViewPager viewPager;

    private Location location;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);

        Toolbar toolbar = (Toolbar) findViewById(R.id.wizard_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setIcon(R.drawable.actionbar_icon_with_space);
        }

        viewPager = (ViewPager) findViewById(R.id.wizard_pager);
        setupViewPager(viewPager);
        TabLayout tabDotsLayout = (TabLayout) findViewById(R.id.wizard_tab_dots);
        tabDotsLayout.setupWithViewPager(viewPager, true);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                switch (position) {
                    case 0:
                        getSupportActionBar().setTitle(getResources().getString(R.string.set_location_name));
                        break;
                    case 1:
                        getSupportActionBar().setTitle(getResources().getString(R.string.set_coordinates));
                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        location = new Location();
        repository = new Repository();
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddLocationFragment());
        adapter.addFragment(new MapsFragment());
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNameDefined(String name) {
        location.setName(name);
        viewPagerScrollToNext();
    }

    private void viewPagerScrollToNext() {
        int size = viewPager.getAdapter().getCount();
        if (viewPager.getCurrentItem() < size - 1) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        } else {
            repository.createLocation(location);
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() > 0) {
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        } else {
            finish();
        }
    }

    @Override
    public void onCoordinateObtained(double latitude, double longitude) {
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        viewPagerScrollToNext();
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }

    }
}
