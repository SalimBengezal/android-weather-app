package practice.dsr.ru.weather.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.activities.MainActivity;
import practice.dsr.ru.weather.activities.WeatherDetails;
import practice.dsr.ru.weather.adapters.LocationArrayAdapter;
import practice.dsr.ru.weather.callbacks.UpdateCaller;
import practice.dsr.ru.weather.callbacks.UpdateListener;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class FavoriteCitiesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private Repository repository;
    private ListView lvLocations;
    private UpdateCaller updateCaller;
    private SwipeRefreshLayout swipeRefreshLayout;
    private final UpdateListener listener = new UpdateListener() {
        @Override
        public void onUpdated() {
            swipeRefreshLayout.setRefreshing(false);
        }
    };

    public FavoriteCitiesFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        repository = new Repository();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            updateCaller = (UpdateCaller) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement onUpdateForecast");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite_cities, container, false);

        List<Location> favouriteLocations = repository.findFavouriteLocations();
        if (favouriteLocations.isEmpty()) {
            RelativeLayout rlMain = new RelativeLayout(getContext());
            rlMain.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
            TextView tvInfo = new TextView(getContext());
            tvInfo.setText(getResources().getString(R.string.no_favourite_cities_info));
            tvInfo.setGravity(Gravity.CENTER);
            RelativeLayout.LayoutParams viewLayoutParams = new RelativeLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT);
            int hMargin = (int) getResources().getDimension(R.dimen.activity_vertical_margin);
            int wMargin = (int) getResources().getDimension(R.dimen.activity_horizontal_margin);
            viewLayoutParams.setMargins(wMargin, hMargin, wMargin, hMargin);
            viewLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            rlMain.addView(tvInfo, viewLayoutParams);
            return rlMain;
        } else {
            lvLocations = (ListView) view.findViewById(R.id.fragments_favourites_list);
            LocationArrayAdapter adapter = new LocationArrayAdapter(getContext(), favouriteLocations);
            lvLocations.setAdapter(adapter);
            lvLocations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intentDetails = new Intent(getContext(), WeatherDetails.class);
                    Location location = (Location) lvLocations.getAdapter().getItem(i);
                    if (location != null) {
                        intentDetails.putExtra("id", location.getId());
                        getActivity().startActivityForResult(intentDetails, MainActivity.REQUEST_REFRESH_LIST);
                    }
                }
            });
            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.fragments_favourites_refresher);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.setColorSchemeResources(R.color.lightBlue, R.color.lightGreen, R.color.lightOrange);
        }
        return view;
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                updateCaller.onUpdateForecast(listener);
            }
        });
    }

}
