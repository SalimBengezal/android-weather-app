package practice.dsr.ru.weather.db;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.IOException;
import java.sql.SQLException;

import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Forecast;
import practice.dsr.ru.weather.db.model.Location;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes = new Class[]{Location.class, CurrentWeather.class, Forecast.class};
    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile("ormlite_config.txt", classes);
    }
}
