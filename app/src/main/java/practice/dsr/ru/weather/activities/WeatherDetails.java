package practice.dsr.ru.weather.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import practice.dsr.ru.weather.Converter;
import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Forecast;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;

public class WeatherDetails extends AppCompatActivity implements View.OnClickListener {

    private TextView tvDatetime;
    private TextView tvCityName;

    private ImageView ivWeatherIcon;
    private TextView tvWeatherTemperature;
    private TextView tvDescription;

    private TextView tvForecast1DayTime;
    private ImageView ivForecast1Icon;
    private TextView tvForecast1Temperature;

    private TextView tvForecast2DayTime;
    private ImageView ivForecast2Icon;
    private TextView tvForecast2Temperature;

    private TextView tvForecast3DayTime;
    private ImageView ivForecast3Icon;
    private TextView tvForecast3Temperature;

    private TextView tvForecast4DayTime;
    private ImageView ivForecast4Icon;
    private TextView tvForecast4Temperature;

    private TextView tvWind;
    private TextView tvPressure;
    private TextView tvHumidity;

    private Button btnDelete;
    private Repository repository;
    private Location location;

    private SharedPreferences prefs;

    private static String formatDouble(double d) {
        if (d == (long) d)
            return String.format(Locale.getDefault(), "%d", (long) d);
        else
            return String.format("%s", d);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_details);
        prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        initializeViews();

        repository = new Repository();
        int id = getIntent().getIntExtra("id", -1);
        if (id != -1) {

            location = repository.findLocationById(id);
            CurrentWeather current = repository.getCurrentWeather(location);
            List<Forecast> forecasts = new ArrayList<>(repository.getForecasts(location));

            tvDatetime.setText(getDateString(current.getDateTime()));
            tvCityName.setText(location.getName());

            setImageFromAssetsToImageView(ivWeatherIcon, current.getIcon());
            tvWeatherTemperature.setText(getTemperature(current.getTemperature()));
            tvDescription.setText(getDescription(current.getDescription()));

            tvHumidity.setText(getHumidityString(current.getHumidity()));
            tvPressure.setText(getPressureString(current.getPressure()));
            tvWind.setText(getWindString(current.getWindDegree(), current.getWindSpeed()));

            Forecast forecast1 = forecasts.get(0);
            tvForecast1DayTime.setText(getTimeOfDay(forecast1.getDateTime()));
            setImageFromAssetsToImageView(ivForecast1Icon, forecast1.getIcon());
            tvForecast1Temperature.setText(getTemperature(forecast1.getTemperature()));

            Forecast forecast2 = forecasts.get(1);
            tvForecast2DayTime.setText(getTimeOfDay(forecast2.getDateTime()));
            setImageFromAssetsToImageView(ivForecast2Icon, forecast2.getIcon());
            tvForecast2Temperature.setText(getTemperature(forecast2.getTemperature()));

            Forecast forecast3 = forecasts.get(2);
            tvForecast3DayTime.setText(getTimeOfDay(forecast3.getDateTime()));
            setImageFromAssetsToImageView(ivForecast3Icon, forecast3.getIcon());
            tvForecast3Temperature.setText(getTemperature(forecast3.getTemperature()));

            Forecast forecast4 = forecasts.get(3);
            tvForecast4DayTime.setText(getTimeOfDay(forecast4.getDateTime()));
            setImageFromAssetsToImageView(ivForecast4Icon, forecast4.getIcon());
            tvForecast4Temperature.setText(getTemperature(forecast4.getTemperature()));

            btnDelete.setOnClickListener(this);
        }
    }

    private String getDescription(String description) {
        String[] weatherDescriptions = getResources().getStringArray(R.array.weather_descriptions);
        switch (description) {
            case "clear sky":
                return weatherDescriptions[0];
            case "few clouds":
                return weatherDescriptions[1];
            case "scattered clouds":
                return weatherDescriptions[2];
            case "broken clouds":
                return weatherDescriptions[3];
            case "shower rain":
                return weatherDescriptions[4];
            case "rain":
                return weatherDescriptions[5];
            case "thunderstorm":
                return weatherDescriptions[6];
            case "snow":
                return weatherDescriptions[7];
            case "mist":
                return weatherDescriptions[8];
            default:
                return "";
        }
    }

    private void setImageFromAssetsToImageView(ImageView imageView, String path) {
        int imageResource = getResources().getIdentifier("drawable/ic_" + path, null, getPackageName());
        imageView.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), imageResource));
    }

    private String getTemperature(double temperature) {
        String type = prefs.getString(SettingsActivity.TEMP_KEY, "");
        boolean isCelsius = false;
        if (type.equals("1")) {
            isCelsius = true;
        }
        String sign = "";
        String unit;
        if (!isCelsius) {
            temperature = Converter.celsiusToFahrenheit(temperature);
            unit = getString(R.string.fahrenheit);
        } else {
            unit = getString(R.string.celsius);
        }
        if (temperature > 0) {
            sign = "+";
        }
        return sign + String.format(Locale.getDefault(), "%.0f", temperature) + unit;
    }

    private String getWindString(double degree, double speed) {
        String direction = "";
        String[] directions = getResources().getStringArray(R.array.wind_directions);
        if (degree > 348.75 || degree <= 11.25)
            direction = directions[0];
        else if (degree <= 33.75)
            direction = directions[1];
        else if (degree <= 56.25)
            direction = directions[2];
        else if (degree <= 78.75)
            direction = directions[3];
        else if (degree <= 101.25)
            direction = directions[4];
        else if (degree <= 123.75)
            direction = directions[5];
        else if (degree <= 146.25)
            direction = directions[6];
        else if (degree <= 168.75)
            direction = directions[7];
        else if (degree <= 191.25)
            direction = directions[8];
        else if (degree <= 213.75)
            direction = directions[9];
        else if (degree <= 236.25)
            direction = directions[10];
        else if (degree <= 258.75)
            direction = directions[11];
        else if (degree <= 281.25)
            direction = directions[12];
        else if (degree <= 303.75)
            direction = directions[13];
        else if (degree <= 326.25)
            direction = directions[14];
        else if (degree <= 348.75)
            direction = directions[15];

        return String.format(Locale.getDefault(), "%s: %s, %s %s", getString(R.string.wind), direction, formatDouble(speed), getString(R.string.speed_unit));
    }

    private String getPressureString(double pressure) {
        return String.format(Locale.getDefault(), "%s: %.0f %s", getString(R.string.pressure), pressure, getString(R.string.mmHg));
    }

    private String getHumidityString(int humidity) {
        return String.format("%s: %s%%", getString(R.string.humidity), humidity);
    }

    private String getTimeOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        switch (hourOfDay) {
            case 0:
                return getString(R.string.night);
            case 6:
                return getString(R.string.morning);
            case 12:
                return getString(R.string.day);
            case 18:
                return getString(R.string.evening);
        }
        return "";
    }

    private String getDateString(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int year = calendar.get(Calendar.YEAR);
        String[] weekDays = new DateFormatSymbols(Locale.getDefault()).getWeekdays();
        String[] monthString = new DateFormatSymbols(Locale.getDefault()).getMonths();
        return String.format("%s, %s %s %s", weekDays[dayOfWeek], day, monthString[month], year);
    }

    private void initializeViews() {
        tvDatetime = (TextView) findViewById(R.id.weather_details_date_time);
        tvCityName = (TextView) findViewById(R.id.weather_details_city_name);
        ivWeatherIcon = (ImageView) findViewById(R.id.weather_details_weather_icon);
        tvWeatherTemperature = (TextView) findViewById(R.id.weather_details_weather_temperature);
        tvDescription = (TextView) findViewById(R.id.weather_details_description);
        tvForecast1DayTime = (TextView) findViewById(R.id.weather_details_forecast_1_day_time);
        ivForecast1Icon = (ImageView) findViewById(R.id.weather_details_forecast_1_icon);
        tvForecast1Temperature = (TextView) findViewById(R.id.weather_details_forecast_1_temperature);
        tvForecast2DayTime = (TextView) findViewById(R.id.weather_details_forecast_2_day_time);
        ivForecast2Icon = (ImageView) findViewById(R.id.weather_details_forecast_2_icon);
        tvForecast2Temperature = (TextView) findViewById(R.id.weather_details_forecast_2_temperature);
        tvForecast3DayTime = (TextView) findViewById(R.id.weather_details_forecast_3_day_time);
        ivForecast3Icon = (ImageView) findViewById(R.id.weather_details_forecast_3_icon);
        tvForecast3Temperature = (TextView) findViewById(R.id.weather_details_forecast_3_temperature);
        tvForecast4DayTime = (TextView) findViewById(R.id.weather_details_forecast_4_day_time);
        ivForecast4Icon = (ImageView) findViewById(R.id.weather_details_forecast_4_icon);
        tvForecast4Temperature = (TextView) findViewById(R.id.weather_details_forecast_4_temperature);
        tvWind = (TextView) findViewById(R.id.weather_details_wind);
        tvPressure = (TextView) findViewById(R.id.weather_details_pressure);
        tvHumidity = (TextView) findViewById(R.id.weather_details_humidity);
        btnDelete = (Button) findViewById(R.id.weather_details_btn_delete);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.weather_details_btn_delete) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.dialog_delete_title));
            builder.setMessage(getString(R.string.dialog_delete_message));
            String positiveText = getString(android.R.string.yes);
            builder.setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    repository.deleteLocation(location);
                    setResult(RESULT_OK);
                    finish();
                }
            });
            String negativeText = getString(android.R.string.cancel);
            builder.setNegativeButton(negativeText, null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

}
