package practice.dsr.ru.weather.fragments.wizard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import practice.dsr.ru.weather.R;

public class AddLocationFragment extends Fragment implements View.OnClickListener {

    private LocationNameListener locationNameListener;
    private EditText etName;
    private TextInputLayout til;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationNameListener = (LocationNameListener) getContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wizard_fragment_add_location, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(getResources().getString(R.string.set_location_name));
        etName = (EditText) view.findViewById(R.id.wizard_add_location_name);
        til = (TextInputLayout) view.findViewById(R.id.wizard_add_location_name_input_layout);
        Button btnAdd = (Button) view.findViewById(R.id.wizard_add_location_btn);
        btnAdd.setOnClickListener(this);
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                til.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        String text = etName.getText().toString();
        if (text.trim().length() == 0) {
            til.setError(getResources().getString(R.string.error_message_empty_city_name));
        } else if (text.trim().length() < 2) {
            til.setError(getResources().getString(R.string.error_message_too_short_name));
        } else {
            locationNameListener.onNameDefined(text);

            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            View focusView = getActivity().getCurrentFocus();
            assert focusView != null;
            inputManager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public interface LocationNameListener {
        void onNameDefined(String name);
    }


}
