package practice.dsr.ru.weather.callbacks;

public interface UpdateCaller {
    void onUpdateForecast(UpdateListener listener);

    void onUpdateList();
}

