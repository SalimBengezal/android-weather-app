package practice.dsr.ru.weather.callbacks;

public interface UpdateListener {
    void onUpdated();
}
