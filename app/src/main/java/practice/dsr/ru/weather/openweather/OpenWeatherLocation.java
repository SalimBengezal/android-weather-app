package practice.dsr.ru.weather.openweather;

import java.util.ArrayList;
import java.util.List;

public class OpenWeatherLocation {

    public int cityCode;
    public String name;
    public double latitude;
    public double longitude;
    public String country;

    public OpenWeatherData now = new OpenWeatherData();
    public List<OpenWeatherData> forecasts = new ArrayList<>();

}

