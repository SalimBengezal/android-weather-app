package practice.dsr.ru.weather.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.List;
import java.util.Locale;

import practice.dsr.ru.weather.Converter;
import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.activities.SettingsActivity;
import practice.dsr.ru.weather.callbacks.UpdateCaller;
import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;

public class LocationArrayAdapter extends ArrayAdapter<Location> {

    private final UpdateCaller updateCaller;
    private Repository repository = null;
    private SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());

    public LocationArrayAdapter(Context context, List<Location> locations) {
        super(context, 0, locations);
        repository = new Repository();
        updateCaller = (UpdateCaller) getContext();
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Location location = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_city, parent, false);
        }
        TextView tvName = (TextView) convertView.findViewById(R.id.items_city_name);
        TextView tvDescription = (TextView) convertView.findViewById(R.id.items_city_description);
        final ToggleButton tbFavourite = (ToggleButton) convertView.findViewById(R.id.items_city_fav_toggle);
        if (location != null) {
            tvName.setText(location.getName());
            CurrentWeather currentWeather = repository.getCurrentWeather(location);
            if (currentWeather != null) {
                tvDescription.setText(String.format("%s: %s", getContext().getString(R.string.current_temperature), getTemperature(currentWeather.getTemperature())));
            } else {
                tvDescription.setText("");
            }
            tbFavourite.setOnCheckedChangeListener(null);
            tbFavourite.setChecked(location.isFavourite());
            if (location.isFavourite()) {
                tbFavourite.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star_filled));
            } else {
                tbFavourite.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star_bordered));
            }
            tbFavourite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    Location locList = getItem(position);
                    if (locList != null) {
                        Location loc = repository.findLocationById(locList.getId());
                        loc.setFavourite(isChecked);
                        repository.updateLocation(loc);
                        if (isChecked) {
                            tbFavourite.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star_filled));
                        } else {
                            tbFavourite.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.star_bordered));
                        }
                        updateCaller.onUpdateList();
                    }
                }
            });
        }
        return convertView;

    }

    private String getTemperature(double temperature) {
        String type = prefs.getString(SettingsActivity.TEMP_KEY, "");
        boolean isCelsius = false;
        if (type.equals("1")) {
            isCelsius = true;
        }
        String sign = "";
        String unit;
        if (!isCelsius) {
            temperature = Converter.celsiusToFahrenheit(temperature);
            unit = getContext().getString(R.string.fahrenheit);
        } else {
            unit = getContext().getString(R.string.celsius);
        }
        if (temperature > 0) {
            sign = "+";
        }
        return sign + String.format(Locale.getDefault(), "%.0f", temperature) + unit;
    }

}
