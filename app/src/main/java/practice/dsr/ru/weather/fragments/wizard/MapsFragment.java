package practice.dsr.ru.weather.fragments.wizard;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Locale;

import practice.dsr.ru.weather.R;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, View.OnClickListener {

    private static final double LOCATION_LATITUDE = 55.75222;
    private static final double LOCATION_LONGITUDE = 37.61556;
    private static final String LOCATION_NAME = "Moscow";
    private CoordinateListener coordinateListener;
    private GoogleMap mMap;
    private LatLng location;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.wizard_map);
        mapFragment.getMapAsync(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.wizard_fragment_maps, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.wizard_fab);
        fab.setOnClickListener(this);
        coordinateListener = (CoordinateListener) getContext();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        location = new LatLng(LOCATION_LATITUDE, LOCATION_LONGITUDE);
        mMap.addMarker(new MarkerOptions().position(location).title(LOCATION_NAME));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        mMap.moveCamera(CameraUpdateFactory.zoomTo(12));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        String message = getString(R.string.latitude) + String.format(Locale.getDefault(), ": %1$,.3f ", latLng.latitude) + getString(R.string.longitude) + String.format(Locale.getDefault(), ": %1$,.3f", latLng.longitude);
        mMap.addMarker(new MarkerOptions().position(latLng).title(message));
        this.location = latLng;
    }

    @Override
    public void onClick(View view) {
        if (location != null) {
            coordinateListener.onCoordinateObtained(location.latitude, location.longitude);
        }
    }

    public interface CoordinateListener {
        void onCoordinateObtained(double latitude, double longitude);
    }
}
