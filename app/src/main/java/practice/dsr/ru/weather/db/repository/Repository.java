package practice.dsr.ru.weather.db.repository;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import practice.dsr.ru.weather.db.DatabaseHelper;
import practice.dsr.ru.weather.db.HelperFactory;
import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Forecast;
import practice.dsr.ru.weather.db.model.Location;

public class Repository {

    private final DatabaseHelper helper = HelperFactory.getHelper();

    public Repository() {
    }

    public int createLocation(Location location) {
        int index = -1;
        try {
            index = helper.getLocationDao().create(location);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return index;
    }

    public void updateLocation(Location location) {
        try {
            helper.getLocationDao().update(location);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int deleteLocation(Location location) {
        int index = -1;
        try {
            Collection<Forecast> forecasts = getForecasts(location);
            assert forecasts != null;
            for (Forecast forecast : forecasts) {
                helper.getForecastDao().deleteById(forecast.getId());
            }
            helper.getWeatherDao().deleteById(getCurrentWeather(location).getId());
            index = helper.getLocationDao().delete(location);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return index;
    }

    public Location findLocationById(int id) {
        Location location = null;
        try {
            location = helper.getLocationDao().queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return location;
    }

    public List<Location> findAllLocations() {
        List<Location> locations = null;
        try {
            locations = helper.getLocationDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public CurrentWeather getCurrentWeather(Location location) {
        try {
            return HelperFactory.getHelper().getWeatherDao().queryBuilder().where().eq(CurrentWeather.FIELD_LOCATION_ID, location.getId()).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setCurrentWeather(Location location, CurrentWeather currentWeather) {
        clearCurrentWeather(location);
        currentWeather.setLocationId(location.getId());
        try {
            HelperFactory.getHelper().getWeatherDao().create(currentWeather);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void clearCurrentWeather(Location location) {
        CurrentWeather currentWeather = getCurrentWeather(location);
        if (currentWeather != null) {
            try {
                HelperFactory.getHelper().getWeatherDao().deleteById(currentWeather.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Location> findFavouriteLocations() {
        List<Location> locations = null;
        try {
            locations = helper.getLocationDao().queryForEq(Location.FIELD_IS_FAVOURITE, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return locations;
    }

    public Collection<Forecast> getForecasts(Location location) {
        try {
            return HelperFactory.getHelper().getForecastDao().queryBuilder().where().eq(Forecast.FIELD_LOCATION_ID, location.getId()).query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setForecasts(Location location, Collection<Forecast> forecastCollection) {
        clearForecasts(location);
        for (Forecast forecast : forecastCollection) {
            addForecastToLocation(location, forecast);
        }
    }

    private void addForecastToLocation(Location location, Forecast forecast) {
        try {
            forecast.setLocationId(location.getId());
            HelperFactory.getHelper().getForecastDao().create(forecast);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeForecastFromCollection(Location location, Forecast forecast) {
        Collection<Forecast> forecasts = getForecasts(location);
        assert forecasts != null;
        for (Forecast weather : forecasts) {
            if (weather.getId() == forecast.getId()) {
                try {
                    HelperFactory.getHelper().getForecastDao().deleteById(forecast.getId());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private void clearForecasts(Location location) {
        Collection<Forecast> forecasts = getForecasts(location);
        assert forecasts != null;
        for (Forecast forecast : forecasts) {
            try {
                HelperFactory.getHelper().getForecastDao().deleteById(forecast.getId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
