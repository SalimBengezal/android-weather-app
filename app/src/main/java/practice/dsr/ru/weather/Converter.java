package practice.dsr.ru.weather;

public class Converter {
    public static double kelvinToCelsius(double temperature) {
        return (temperature - 273.15);
    }

    public static double celsiusToFahrenheit(double celsius) {
        return celsius * 9 / 5 + 32;
    }
}
