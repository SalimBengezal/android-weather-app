package practice.dsr.ru.weather;


import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import practice.dsr.ru.weather.db.model.CurrentWeather;
import practice.dsr.ru.weather.db.model.Forecast;
import practice.dsr.ru.weather.db.model.Location;
import practice.dsr.ru.weather.db.repository.Repository;
import practice.dsr.ru.weather.openweather.OpenWeatherData;
import practice.dsr.ru.weather.openweather.OpenWeatherLocation;
import practice.dsr.ru.weather.openweather.OpenWeatherMapManager;

public class WeatherUpdater {

    private static final String TAG = WeatherUpdater.class.getName();
    private static WeatherUpdater instance;
    private Repository repository;
    private String apiKey;

    private WeatherUpdater(Context context) {
        repository = new Repository();
        apiKey = context.getString(R.string.open_weather_api_key);
    }

    public static WeatherUpdater getInstance(Context context) {
        if (instance == null) {
            instance = new WeatherUpdater(context);
        }
        return instance;
    }

    public void update() {
        Log.d(TAG, "UPDATING");
        List<Location> locations = repository.findAllLocations();
        if (locations != null) {
            for (Location location : locations) {
                updateCurrentWeatherInLocation(location);
                updateForecastInLocation(location);
                repository.updateLocation(location);
            }
        }
    }

    private void updateForecastInLocation(Location location) {
        String urlForecast = OpenWeatherMapManager.getUrlForecast(location.getLatitude(), location.getLongitude(), apiKey);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlForecast).build();
        OpenWeatherLocation forecastResponse = null;
        try {
            Response response = client.newCall(request).execute();
            JSONObject jsonForecast = new JSONObject(response.body().string());
            forecastResponse = OpenWeatherMapManager.jsonToForecast(jsonForecast);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        assert forecastResponse != null;
        location.setCityCode(forecastResponse.cityCode);
        location.setCountry(forecastResponse.country);
        location.setLatitude(forecastResponse.latitude);
        location.setLongitude(forecastResponse.longitude);
        List<OpenWeatherData> weatherList = forecastResponse.forecasts;
        List<Forecast> forecasts = new ArrayList<>();
        int i = 0;
        int k = 0;
        Calendar calendar = Calendar.getInstance();
        while (i < 4 && k < weatherList.size()) {
            OpenWeatherData weather = weatherList.get(k++);
            Forecast forecast = new Forecast();
            if (weather != null) {
                calendar.setTime(weather.dateTime);
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                if (hour == 0 || hour == 6 || hour == 12 || hour == 18) {
                    i++;
                } else
                    continue;
            } else
                return;
            forecast.setDateTime(weather.dateTime);
            forecast.setDescription(weather.description);
            forecast.setHumidity(weather.humidity);
            forecast.setIcon(weather.icon);
            forecast.setPressure(weather.pressure);
            forecast.setState(weather.state);
            forecast.setWindDegree(weather.windDegree);
            forecast.setWindSpeed(weather.windSpeed);
            forecast.setTemperature(weather.temperature);
            forecast.setLocationId(location.getId());
            forecasts.add(forecast);
        }
        repository.setForecasts(location, forecasts);
        forecasts.clear();
    }

    private void updateCurrentWeatherInLocation(Location location) {
        String urlCurrent = OpenWeatherMapManager.getUrlCurrentWeather(location.getLatitude(), location.getLongitude(), apiKey);
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urlCurrent).build();
        OpenWeatherLocation currentWeatherResponse = null;
        try {
            Response response = client.newCall(request).execute();
            JSONObject jsonCurrent = new JSONObject(response.body().string());
            currentWeatherResponse = OpenWeatherMapManager.jsonToWeather(jsonCurrent);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        assert currentWeatherResponse != null;
        location.setCityCode(currentWeatherResponse.cityCode);
        location.setCountry(currentWeatherResponse.country);
        location.setLatitude(currentWeatherResponse.latitude);
        location.setLongitude(currentWeatherResponse.longitude);
        OpenWeatherData weather = currentWeatherResponse.now;
        CurrentWeather currentWeather = new CurrentWeather();
        currentWeather.setTemperature(weather.temperature);
        currentWeather.setWindSpeed(weather.windSpeed);
        currentWeather.setWindDegree(weather.windDegree);
        currentWeather.setDateTime(weather.dateTime);
        currentWeather.setDescription(weather.description);
        currentWeather.setHumidity(weather.humidity);
        currentWeather.setIcon(weather.icon);
        currentWeather.setPressure(weather.pressure);
        currentWeather.setState(weather.state);
        currentWeather.setLocationId(location.getId());
        repository.updateLocation(location);
        repository.setCurrentWeather(location, currentWeather);
    }

}
