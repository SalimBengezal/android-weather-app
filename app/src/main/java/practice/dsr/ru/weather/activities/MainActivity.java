package practice.dsr.ru.weather.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import practice.dsr.ru.weather.R;
import practice.dsr.ru.weather.WeatherUpdater;
import practice.dsr.ru.weather.callbacks.UpdateCaller;
import practice.dsr.ru.weather.callbacks.UpdateListener;
import practice.dsr.ru.weather.fragments.AllCitiesFragment;
import practice.dsr.ru.weather.fragments.FavoriteCitiesFragment;

public class MainActivity extends AppCompatActivity implements UpdateCaller {

    public static final int REQUEST_REFRESH_LIST = 2;
    public static final int REQUEST_UPDATE = 1;
    private ProgressDialog progressDialog;
    private ViewPager viewPager;
    private WeatherUpdater updater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            getSupportActionBar().setTitle(getResources().getString(R.string.locations));
            getSupportActionBar().setIcon(R.drawable.actionbar_icon_with_space);
        }

        viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.main_tabs);
        tabLayout.setupWithViewPager(viewPager);
        updater = WeatherUpdater.getInstance(getApplicationContext());

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage(getString(R.string.updating_forecasts_message));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!haveAccessToInternet()) {
                    showAlertDialogNoInternetConnection();
                } else {
                    UpdateListener mListener = new UpdateListener() {
                        @Override
                        public void onUpdated() {
                            progressDialog.dismiss();
                        }
                    };
                    progressDialog.show();
                    new UpdaterAsyncTask(mListener).execute();
                }
            }
        }, 2000);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AllCitiesFragment(), getResources().getString(R.string.all));
        adapter.addFragment(new FavoriteCitiesFragment(), getResources().getString(R.string.favorites));
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add:
                Intent intent = new Intent(this, WizardActivity.class);
                startActivityForResult(intent, REQUEST_UPDATE);
                break;
            case R.id.menu_settings:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivityForResult(settingsIntent, REQUEST_REFRESH_LIST);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_UPDATE:
                    if (!haveAccessToInternet()) {
                        showAlertDialogNoInternetConnection();
                    } else {
                        UpdateListener mListener = new UpdateListener() {
                            @Override
                            public void onUpdated() {
                                progressDialog.dismiss();
                            }
                        };
                        progressDialog.show();
                        new UpdaterAsyncTask(mListener).execute();
                    }
                    break;
                case REQUEST_REFRESH_LIST:
                    onUpdateList();
                    break;
            }
        }
    }

    @Override
    public void onUpdateForecast(UpdateListener listener) {
        if (!haveAccessToInternet()) {
            listener.onUpdated();
            showAlertDialogNoInternetConnection();
        } else {
            new UpdaterAsyncTask(listener).execute();
        }
    }

    @Override
    public void onUpdateList() {
        int pos = viewPager.getCurrentItem();
        setupViewPager(viewPager);
        viewPager.setCurrentItem(pos);
    }

    private boolean haveAccessToInternet() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    private void showAlertDialogNoInternetConnection() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.no_internet_connection_message));
        builder.setTitle(getString(R.string.no_internet_connection_title));
        builder.setPositiveButton(getString(android.R.string.ok), null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class UpdaterAsyncTask extends AsyncTask<Void, Void, Void> {
        final UpdateListener listener;

        UpdaterAsyncTask(UpdateListener listener) {
            this.listener = listener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            updater.update();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (listener != null)
                listener.onUpdated();
            onUpdateList();
        }
    }

}


