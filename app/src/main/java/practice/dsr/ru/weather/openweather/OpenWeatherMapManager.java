package practice.dsr.ru.weather.openweather;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import practice.dsr.ru.weather.Converter;

public class OpenWeatherMapManager {

    private static final String FORECAST_COUNT = "cnt";
    private static final String CITY = "city";
    private static final String CITY_ID = "id";
    private static final String CITY_NAME = "name";
    private static final String COUNTRY = "country";
    private static final String COORDINATES = "coord";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lon";
    private static final String LIST = "list";
    private static final String MAIN = "main";
    private static final String WEATHER_DESCRIPTION = "description";
    private static final String WEATHER_ICON = "icon";
    private static final String WEATHER_MAIN = "main";
    private static final String WEATHER = "weather";
    private static final String TEMPERATURE = "temp";
    private static final String PRESSURE = "pressure";
    private static final String HUMIDITY = "humidity";
    private static final String WIND_SPEED = "speed";
    private static final String WIND_DEGREE = "deg";
    private static final String DATE_TIME = "dt";
    private static final String WIND = "wind";
    private static final String SYS = "sys";
    private static final String APP_ID = "APPID";

    public static OpenWeatherLocation jsonToForecast(JSONObject jsonObject) {
        OpenWeatherLocation weatherData = new OpenWeatherLocation();
        try {
            int forecastCount = jsonObject.getInt(FORECAST_COUNT);
            if (jsonObject.has(CITY)) {
                JSONObject city = jsonObject.getJSONObject(CITY);
                weatherData.cityCode = city.getInt(CITY_ID);
                weatherData.country = city.getString(COUNTRY);
                weatherData.name = city.getString(CITY_NAME);
                if (city.has(COORDINATES)) {
                    JSONObject jsonCoordinates = city.getJSONObject(COORDINATES);
                    weatherData.latitude = jsonCoordinates.getDouble(LATITUDE);
                    weatherData.longitude = jsonCoordinates.getDouble(LONGITUDE);
                }
            }
            if (jsonObject.has(LIST)) {
                JSONArray jsonList = jsonObject.getJSONArray(LIST);
                JSONObject weatherJson;
                for (int i = 0; i < forecastCount; i++) {
                    OpenWeatherData forecast = new OpenWeatherData();
                    weatherJson = jsonList.getJSONObject(i);
                    forecast.dateTime = new Date((long) weatherJson.getInt(DATE_TIME) * 1000);
                    if (weatherJson.has(MAIN)) {
                        JSONObject jsonWeatherMain = weatherJson.getJSONObject(MAIN);
                        forecast.temperature = Converter.kelvinToCelsius(jsonWeatherMain.getDouble(TEMPERATURE));
                        forecast.humidity = jsonWeatherMain.getInt(HUMIDITY);
                        forecast.pressure = getMmHg(jsonWeatherMain.getDouble(PRESSURE));
                    }
                    if (weatherJson.has(WIND)) {
                        JSONObject jsonWind = weatherJson.getJSONObject(WIND);
                        forecast.windDegree = jsonWind.getDouble(WIND_DEGREE);
                        forecast.windSpeed = jsonWind.getDouble(WIND_SPEED);
                    }
                    if (weatherJson.has(WEATHER)) {
                        JSONArray jsonWeatherArray = weatherJson.getJSONArray(WEATHER);
                        JSONObject jsonWeatherObject = jsonWeatherArray.getJSONObject(0);
                        forecast.icon = jsonWeatherObject.getString(WEATHER_ICON);
                        forecast.description = jsonWeatherObject.getString(WEATHER_DESCRIPTION);
                        forecast.state = jsonWeatherObject.getString(WEATHER_MAIN);
                    }
                    weatherData.forecasts.add(forecast);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weatherData;
    }

    public static OpenWeatherLocation jsonToWeather(JSONObject jsonObject) {
        OpenWeatherLocation weatherData = new OpenWeatherLocation();
        OpenWeatherData weather = new OpenWeatherData();
        try {
            weatherData.cityCode = jsonObject.getInt(CITY_ID);
            weatherData.name = jsonObject.getString(CITY_NAME);
            weather.dateTime = new Date((long) jsonObject.getInt(DATE_TIME) * 1000);

            if (jsonObject.has(SYS)) {
                JSONObject sys = jsonObject.getJSONObject(SYS);
                weatherData.country = sys.getString(COUNTRY);
            }
            if (jsonObject.has(COORDINATES)) {
                JSONObject jsonCoordinates = jsonObject.getJSONObject(COORDINATES);
                weatherData.latitude = jsonCoordinates.getDouble(LATITUDE);
                weatherData.longitude = jsonCoordinates.getDouble(LONGITUDE);
            }
            if (jsonObject.has(MAIN)) {
                JSONObject jsonWeatherMain = jsonObject.getJSONObject(MAIN);
                weather.temperature = Converter.kelvinToCelsius(jsonWeatherMain.getDouble(TEMPERATURE));
                weather.humidity = jsonWeatherMain.getInt(HUMIDITY);
                weather.pressure = getMmHg(jsonWeatherMain.getDouble(PRESSURE));
            }
            if (jsonObject.has(WIND)) {
                JSONObject jsonWind = jsonObject.getJSONObject(WIND);
                if (jsonWind.has(WIND_DEGREE))
                    weather.windDegree = jsonWind.getDouble(WIND_DEGREE);
                if (jsonWind.has(WIND_SPEED))
                    weather.windSpeed = jsonWind.getDouble(WIND_SPEED);
            }
            if (jsonObject.has(WEATHER)) {
                JSONArray jsonWeatherArray = jsonObject.getJSONArray(WEATHER);
                JSONObject jsonWeatherObject = jsonWeatherArray.getJSONObject(0);
                weather.icon = jsonWeatherObject.getString(WEATHER_ICON);
                weather.description = jsonWeatherObject.getString(WEATHER_DESCRIPTION);
                weather.state = jsonWeatherObject.getString(WEATHER_MAIN);
            }
            weatherData.now = weather;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return weatherData;
    }

    private static double getMmHg(double pressurePa) {
        return pressurePa * 76000 / 101325;
    }

    public static String getUrlForecast(double latitude, double longitude, String apiKey) {
        return "http://api.openweathermap.org/data/2.5/forecast?"
                + LATITUDE + "=" + latitude + "&"
                + LONGITUDE + "=" + longitude + "&"
                + APP_ID + "=" + apiKey;
    }

    public static String getUrlCurrentWeather(double latitude, double longitude, String apiKey) {
        return "http://api.openweathermap.org/data/2.5/weather?"
                + LATITUDE + "=" + latitude + "&"
                + LONGITUDE + "=" + longitude + "&"
                + APP_ID + "=" + apiKey;
    }
}
